import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

# Check stopwords
try:
    stopwords.words('english')
except LookupError:
    import nltk
    nltk.download("stopwords")


def miningready(text):
    """
    Fonction de sac de mots, prépare un message afin de le rendre conforme avant de transformer le texte.

    Une première instruction élimine tout les caractères préciaux en filtrant le message d'abord interprété comme un tableau de
    char puis réassemblé en string:
        >>> text = ''.join([char for char in text if char not in '''!"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]"'''])

    Ce message sans caractère spécial est ensuite de nouveau découpé, mais cette fois en mots, puis on filtre les mots
    qui sont des stopwords en utilisant le paquet nltk:
        >>> text = text.split() if words.lower() not in stopwords.words('english')

    Ces mots sont retournés par la fonction sous forme de liste:
        >>> return [word for word in text]

    :param text: Le message non formaté
    :type text: str
    :return: Le message sans ses stopwords et sans caractères spéciaux, sous forme de liste
    :rtype: list
    """
    return [
        word for word in
        ''.join([char for char in text if char not in '''!"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~''']).split() if
        word.lower() not in stopwords.words('english')
    ]  # <-- Tableau de mots


# Import du dataset
messages = pd.read_csv('SMSSpamCollection.txt', sep='\t', header=0, index_col=False, names=['classes', 'message'])

# Séparation des messages (X) et des labels (y) en données d'entrainement (_train) et données de test (_test)
X_train, X_test, y_train, y_test = train_test_split(messages['message'], messages['classes'], shuffle=False)

# On initialise un CountVectoriser avec comme analyseur notre fonction performant le sac de mots
count_vect = CountVectorizer(analyzer=miningready)

# Adaptation puis transformation des données d'entrainement, elle passent alors dans la fonction d'analyse
count_vect.fit(X_train)
X = count_vect.transform(X_train)

# Création d'un modèle de détection de spam utilisant une clasification naïve Bayésienne, on utilise ici un Tfidf
# initialisé à partir des données du CountVectoriser
tfidf_transformer = TfidfTransformer().fit(X)
messages_tfidf = tfidf_transformer.transform(X)
spam_detect_model = MultinomialNB().fit(messages_tfidf, y_train)

# On convertit les datasets de test en des tableau pour pouvoir les itérer sans provoquer d'erreur d'index
# (rendu possible grâce au shuffle=False de la fonction train_test_split - ligne 47)
message_test = X_test.to_list()
label_test = y_test.to_list()
total_guessed = 0
total_failed = 0
data = len(message_test)
for i in range(len(message_test)):
    print()
    # Pour chaque message du jeu de test, on tente de prédire sa classe puis on vérifie si la prédiction est juste.
    message = tfidf_transformer.transform(count_vect.transform([message_test[i]]))
    print(message_test[i])
    predicted = spam_detect_model.predict(message)[0]
    expected = label_test[i]
    print('Predicted: ', predicted)
    print('Expected: ', expected)
    print()
    if predicted == expected:
        total_guessed += 1
    else:
        total_failed += 1

print()
print()
print()
print()
print("Rapport:")
print("Total des données testées:", data)
print("Bonnes prédictions :", total_guessed)
print("Mauvaises prédictions :", total_failed)
print("Pourcentage de réussite :", (total_guessed / data) * 100)
